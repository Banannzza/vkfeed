//
//  UserServiceProtocol.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 10/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol UsersServiceProtocol {
    func userImageURL(forUser userID: String?, completion: @escaping (ServiceResponse<URL>) -> Void)
}
