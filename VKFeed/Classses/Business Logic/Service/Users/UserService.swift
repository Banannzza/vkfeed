//
//  UserService.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 10/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class UsersService: UsersServiceProtocol {
    
    func userImageURL(forUser userID: String?, completion: @escaping (ServiceResponse<URL>) -> Void) {
        guard let token = ServiceLayer.shared.accessToken else { return }
        
        var urlComponents = URLComponents(string: "https://api.vk.com/method/users.get")
        
        urlComponents?.queryItems = [URLQueryItem(name: "access_token", value: token),
                                     URLQueryItem(name: "fields", value: "photo_100"),
                                     URLQueryItem(name: "v", value: "5.87")]
        
        let url = urlComponents?.url
        let session = URLSession.shared
        let request = URLRequest(url: url!)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard error == nil, let data = data else {
                completion(.fail)
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                    let response = json["response"] as? [[String: Any]],
                    let urlString = response.first?["photo_100"] as? String,
                    let url = URL(string: urlString) {
                    completion(.success(url))
                }
            } catch _ {
                completion(.fail)
            }
        })
        
        task.resume()
    }
    
    
}
