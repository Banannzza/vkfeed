//
//  SearchService.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class SearchService: SearchServiceProtocol {
    
    var nextFrom: String?
    var searchText: String!
    weak var searchTask: URLSessionDataTask?
    
    func searchPost(count: Int?, next: String?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void) {
        guard let token = ServiceLayer.shared.accessToken, searchTask == nil else { return }
        
        var urlComponents = URLComponents(string: "https://api.vk.com/method/newsfeed.search")
        
        urlComponents?.queryItems = [ URLQueryItem(name: "q", value: searchText),
                                      URLQueryItem(name: "extended", value: "1"),
                                      URLQueryItem(name: "access_token", value: token),
                                      URLQueryItem(name: "v", value: "5.87")]
        
        if let count = count {
            urlComponents?.queryItems?.append(URLQueryItem(name: "count", value: "\(count)"))
        }
        
        if let next = next {
            urlComponents?.queryItems?.append(URLQueryItem(name: "start_from", value: "\(next)"))
        }
        
        let url = urlComponents?.url
        let session = URLSession.shared
        let request = URLRequest(url: url!)
        searchTask = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] data, response, error in
            guard let strongSelf = self else { return }
            guard error == nil, let data = data else {
                completion(.fail)
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                    let response = json["response"] as? [String: Any],
                    let posts = response["items"] as? [[String: Any]],
                    let groups = response["groups"] as? [[String: Any]],
                    let profiles = response["profiles"] as? [[String: Any]] {
                    let loadedPosts = Post.decode(from: posts)
                    let loadedGroups = Group.decode(from: groups)
                    let loadedProfiles = Profile.decode(from: profiles)
                    strongSelf.nextFrom = response["next_from"] as? String
                    completion(.success((posts: loadedPosts, groups: loadedGroups, profiles: loadedProfiles)))
                }
            } catch _ {
                completion(.fail)
            }
        })
        
        searchTask?.resume()
    }
    
    func searchPost(searchText: String, count: Int?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void) {
        self.searchText = searchText
        searchPost(count: count, next: nil, completion: completion)
    }
    func nextSearchPost(count: Int?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void) {
        searchPost(count: count, next: nextFrom, completion: completion)
    }
}
