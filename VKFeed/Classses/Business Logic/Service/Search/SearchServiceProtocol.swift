//
//  SearchServiceProtocol.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol SearchServiceProtocol {
    func searchPost(searchText: String, count: Int?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void)
    func nextSearchPost(count: Int?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void)
}
