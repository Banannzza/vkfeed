//
//  NewsFeedService.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 10/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class NewsFeedService: NewsFeedServiceProtocol {
    
    var nextFrom: String?
    weak var nextPostTask: URLSessionDataTask?
    weak var lastPostTask: URLSessionDataTask?

    private func loadPost(task: inout URLSessionDataTask?, count: Int?, next: String?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void) {
        guard let token = ServiceLayer.shared.accessToken, task == nil else { return }

        var urlComponents = URLComponents(string: "https://api.vk.com/method/newsfeed.get")
        
        urlComponents?.queryItems = [ URLQueryItem(name: "filters", value: "post"),
                                      URLQueryItem(name: "return_banned", value: "0"),
                                      URLQueryItem(name: "access_token", value: token),
                                      URLQueryItem(name: "v", value: "5.87")]
        
        if let count = count {
            urlComponents?.queryItems?.append(URLQueryItem(name: "count", value: "\(count)"))
        }
        
        if let next = next {
            urlComponents?.queryItems?.append(URLQueryItem(name: "start_from", value: "\(next)"))
        }
        
        let url = urlComponents?.url
        let session = URLSession.shared
        let request = URLRequest(url: url!)
        task = session.dataTask(with: request as URLRequest, completionHandler: { [weak self] data, response, error in
            guard let strongSelf = self else { return }
            guard error == nil, let data = data else {
                completion(.fail)
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                    let response = json["response"] as? [String: Any],
                    let posts = response["items"] as? [[String: Any]],
                    let groups = response["groups"] as? [[String: Any]],
                    let profiles = response["profiles"] as? [[String: Any]] {
                    let loadedPosts = Post.decode(from: posts)
                    let loadedGroups = Group.decode(from: groups)
                    let loadedProfiles = Profile.decode(from: profiles)
                    strongSelf.nextFrom = response["next_from"] as? String
                    completion(.success((posts: loadedPosts, groups: loadedGroups, profiles: loadedProfiles)))
                }
            } catch _ {
                completion(.fail)
            }
        })
        
        task?.resume()
    }
    
    func loadLastPosts(count: Int?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void) {
        loadPost(task: &lastPostTask, count: count, next: nil, completion: completion)
    }
    
    func loadNextPosts(count: Int?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void) {
        loadPost(task: &nextPostTask, count: count, next: nextFrom, completion: completion)
    }
    
}
