//
//  NewsFeedServiceProtocol.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 10/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol NewsFeedServiceProtocol {
    func loadLastPosts(count: Int?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void)
    func loadNextPosts(count: Int?, completion: @escaping (ServiceResponse<(posts: [Post], groups: [Group], profiles: [Profile])>) -> Void)
}
