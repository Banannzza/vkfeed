//
//  ServiceLayer.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 10/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class ServiceLayer {
    
    static let shared = ServiceLayer()
    
    var accessToken: String?
    let newsfeedService: NewsFeedServiceProtocol
    let searchService: SearchServiceProtocol
    let usersService: UsersServiceProtocol
    
    private init() {
        newsfeedService = NewsFeedService()
        usersService = UsersService()
        searchService = SearchService()
    }
}
