//
//  ImageManager.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 10/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class ImageManager {
    
    static let shared = ImageManager()
    
    private var storage: [URL: UIImage]
    
    private init() {
        storage = [URL: UIImage]()
    }
    
    func imageData(for url: URL) -> UIImage? {
        return storage[url]
    }
    
    func save(data: UIImage, forURL url: URL) {
        guard storage[url] == nil else { return }
        storage[url] = data
    }
    
    func clear() {
        storage.removeAll()
    }
}
