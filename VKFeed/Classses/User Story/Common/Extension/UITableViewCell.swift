//
//  UITableViewCell.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static func register(to tableView: UITableView) {
        let identifier = String(describing: self)
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: identifier, bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: identifier)
    }
}
