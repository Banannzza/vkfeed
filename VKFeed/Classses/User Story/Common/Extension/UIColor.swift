//
//  UIColor.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

extension UIColor {
    static let imageGrayTintColor = UIColor(red: 0.6, green: 0.64, blue: 0.68, alpha: 1)
}
