//
//  UICollectionViewCell.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    
    static func register(to collectionView: UICollectionView) {
        let identifier = String(describing: self)
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: identifier, bundle: bundle)
        collectionView.register(nib, forCellWithReuseIdentifier: identifier)
    }
    
}
