//
//  UIImage.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

extension UIImage {
    
    class func makeAvatatImage(from original: UIImage, aspectFilledFor size: CGSize, backgroundColor: UIColor? = .white) -> UIImage? {
        let scaledImage = original.scaleImage(to: size)
        let clipedImage = scaledImage?.clipImage(forSize: size)
        var resultImage = clipedImage
        if let backgroundColor = backgroundColor, backgroundColor != .clear,
            let backgroundImage = UIImage.backgroundImage(backgroundColor: backgroundColor, size: size),
            let clipedImage = clipedImage {
            resultImage = UIImage.combineImages(bottomImage: backgroundImage, topImage: clipedImage)
            return resultImage
        }
        return resultImage
    }
    
    func scaleImage(to newSize: CGSize) -> UIImage? {
        var scaledImageRect = CGRect.zero
        
        let aspectWidth: CGFloat = newSize.width / size.width
        let aspectHeight: CGFloat = newSize.height / size.height
        let aspectRatio = max(aspectWidth, aspectHeight)
        
        scaledImageRect.size.width = size.width * aspectRatio
        scaledImageRect.size.height = size.height * aspectRatio
        scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0
        scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0
        
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: newSize)
            let scaledImage = renderer.image { _ in
                draw(in: scaledImageRect)
            }
            return scaledImage
        } else {
            UIGraphicsBeginImageContextWithOptions(newSize, false, UIScreen.main.scale)
            draw(in: scaledImageRect)
            let scaledImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return scaledImage
        }
        
    }
    
    class func backgroundImage(backgroundColor: UIColor, size: CGSize) -> UIImage? {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: size)
            let backgroundImage = renderer.image { context in
                backgroundColor.setFill()
                context.fill(CGRect(x: 0, y: 0, width: size.width, height: size.height))
            }
            return backgroundImage
        }
        else {
            let layer = CAShapeLayer()
            layer.path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: size.width, height: size.height)).cgPath
            layer.fillColor = backgroundColor.cgColor
            UIGraphicsBeginImageContextWithOptions(size, true, UIScreen.main.scale)
            if let aContext = UIGraphicsGetCurrentContext() {
                layer.render(in: aContext)
            }
            let backgroundImage: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return backgroundImage
        }
    }
    
    class func combineImages(bottomImage: UIImage, topImage: UIImage) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(bottomImage.size, true, UIScreen.main.scale)
        
        bottomImage.draw(in: CGRect(origin: CGPoint.zero, size: bottomImage.size))
        topImage.draw(in: CGRect(origin: CGPoint.zero, size: bottomImage.size))
        
        let combinedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return combinedImage
    }
    
    func clipImage(forSize size: CGSize) -> UIImage? {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: size)
            let clipedImage = renderer.image { _ in
                let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
                UIBezierPath(roundedRect: rect, cornerRadius: size.height / 2).addClip()
                self.draw(in: rect)
                let line = UIBezierPath(ovalIn: rect)
                UIColor(red: 0.67, green: 0.68, blue: 0.7, alpha: 1).setStroke()
                line.lineWidth = 0.5
                line.stroke()
            }
            return clipedImage
        }
        else {
            UIGraphicsBeginImageContextWithOptions(self.size, false, UIScreen.main.scale);
            let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
            UIBezierPath(roundedRect: rect, cornerRadius: size.height / 2).addClip()
            self.draw(in: rect)
            let line = UIBezierPath(ovalIn: rect)
            UIColor(red: 0.67, green: 0.68, blue: 0.7, alpha: 1).setStroke()
            line.lineWidth = 0.5
            line.stroke()
            let clipedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return clipedImage
        }
    }
    
}
