//
//  PhotoCollectionView.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class PhotoCollectionView: UICollectionView {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    // MARK: - Property
    
    var photos = [PhotoAttachment]()
    let cellIdentifier = String(describing: PhotoCollectionCell.self)
    
    // MARK: - Lifecyclce
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dataSource = self
        delegate = self
        PhotoCollectionCell.register(to: self)
    }
    
    // MARK: - Method
    
    func reloadPhotos(with newPhotos: [PhotoAttachment]) {
        guard photos != newPhotos else { return }
        let oldPhotosCount = photos.count
        let viewLayot = collectionViewLayout as! CustomFlowLayout
        viewLayot.numberOfItems = newPhotos.count
        viewLayot.invalidateLayout()
        pageControl.numberOfPages = newPhotos.count
        pageControl.currentPage = 0
        performBatchUpdates({
            photos = newPhotos
            if oldPhotosCount > newPhotos.count {
                let deletePaths = (photos.count..<oldPhotosCount).map { IndexPath(row: $0, section: 0) }
                deleteItems(at: deletePaths)
                let reloadPaths = (0..<photos.count).map { IndexPath(row: $0, section: 0) }
                reloadItems(at: reloadPaths)
            }
            else if oldPhotosCount < photos.count {
                let insertPaths = (oldPhotosCount..<photos.count).map { IndexPath(row: $0, section: 0) }
                insertItems(at: insertPaths)
                let reloadPaths = (0..<oldPhotosCount).map { IndexPath(row: $0, section: 0) }
                reloadItems(at: reloadPaths)
            }
            else {
                let reloadPaths = (0..<photos.count).map { IndexPath(row: $0, section: 0) }
                reloadItems(at: reloadPaths)
            }
        }, completion: nil)
    }
}

// MARK: - UICollectionViewDataSource

extension PhotoCollectionView: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! PhotoCollectionCell
        cell.configure(photo: photos[indexPath.row], size: cell.frame.size)
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension PhotoCollectionView: UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width - 60)
    }
    
}




