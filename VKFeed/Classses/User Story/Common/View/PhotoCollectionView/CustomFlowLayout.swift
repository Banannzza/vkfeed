//
//  CustomFlowLayout.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class CustomFlowLayout: UICollectionViewFlowLayout {
    
    // MARK: - Property
    
    var numberOfItems: Int = 0
    
    lazy var collectionWidth = UIScreen.main.bounds.width - 16
    lazy var collectionHeight = floor(collectionWidth / 1.3)
    
    override func prepare() {
        self.scrollDirection = .horizontal
        self.minimumLineSpacing = 4
        let sectionInset: CGFloat = numberOfItems > 1 ? 12 : 0
        self.sectionInset = UIEdgeInsets(top: 0, left: sectionInset, bottom: 0, right: sectionInset)
        
        var itemWidth: CGFloat = 0
        var itemHeight: CGFloat = 0
        
        if numberOfItems == 1 {
            itemWidth = collectionWidth
            itemHeight = collectionHeight
        }
        else {
            itemWidth = collectionWidth - sectionInset * 2
            itemHeight = collectionHeight - 37
        }
        
        self.itemSize = CGSize(width: itemWidth, height: itemHeight)
        super.prepare()
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        if let cv = self.collectionView {
            
            let cvBounds = cv.bounds
            let halfWidth = cvBounds.size.width * 0.5;
            let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth;
            
            if let attributesForVisibleCells = self.layoutAttributesForElements(in: cvBounds) {
                
                var candidateAttributes : UICollectionViewLayoutAttributes?
                for attributes in attributesForVisibleCells {
                    
                    if attributes.representedElementCategory != UICollectionView.ElementCategory.cell {
                        continue
                    }
                    
                    if let candAttrs = candidateAttributes {
                        
                        let a = attributes.center.x - proposedContentOffsetCenterX
                        let b = candAttrs.center.x - proposedContentOffsetCenterX
                        
                        if fabsf(Float(a)) < fabsf(Float(b)) {
                            candidateAttributes = attributes;
                        }
                        
                    }
                    else {
                        candidateAttributes = attributes;
                        continue;
                    }
                }
                return CGPoint(x : candidateAttributes!.center.x - halfWidth, y : proposedContentOffset.y);
            }
        }
        return super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
    }
}
