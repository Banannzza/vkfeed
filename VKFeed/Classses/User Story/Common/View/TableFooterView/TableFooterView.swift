//
//  TableFooterView.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 10/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class TableFooterView: UIView {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var postNumberLabel: UILabel!
    
    func updatePostCounter(_ count: Int) {
        postNumberLabel.text = "\(count) записей"
        setAnimation(false)
    }
    
    func setAnimation(_ animating: Bool) {
        animating ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
        postNumberLabel.isHidden = animating
    }
}
