//
//  TableHeaderView.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 10/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

protocol TableHeaderViewDelegate: AnyObject {
    func tableHeader(_ header: TableHeaderView, didChageSearchText searchText: String)
}

class TableHeaderView: UIView {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var avatarImageView: AsyncImageView!
    
    var timer: Timer?
    
    weak var delegate: TableHeaderViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchBar.barTintColor = UIColor.clear
        searchBar.backgroundImage = UIImage()
        searchBar.delegate = self
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor(red: 0.0, green: 0.11, blue: 0.24, alpha: 0.06)
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.systemFont(ofSize: 16.0)
    }
    
    @objc func checkSearchBar() {
        if let searchText = searchBar.text {
            delegate?.tableHeader(self, didChageSearchText: searchText)
        }
    }

}

extension TableHeaderView: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(checkSearchBar), userInfo: nil, repeats: false)
    }
}
