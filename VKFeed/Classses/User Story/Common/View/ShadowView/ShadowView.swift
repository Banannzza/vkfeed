//
//  ShadowView.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

@IBDesignable
class ShadowView: UIView {

    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        layer.shadowColor = UIColor(red: 0.39, green: 0.4, blue: 0.44, alpha: 0.07).cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize(width: 0, height: 24)
        layer.shadowRadius = 18
        layer.masksToBounds = false
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.shadowPath = UIBezierPath(rect: frame).cgPath
    }
}
