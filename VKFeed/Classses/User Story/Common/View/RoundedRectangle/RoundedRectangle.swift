//
//  RoundedRectangle.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedRectangle: UIView {
    
    override class var layerClass: AnyClass {
        get {
            return CAShapeLayer.self
        }
    }
    
    override var layer: CAShapeLayer {
        return super.layer as! CAShapeLayer
    }
  
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        layer.drawsAsynchronously = true
        layer.fillColor = UIColor.white.cgColor
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.path = UIBezierPath(roundedRect: frame, cornerRadius: 10).cgPath
    }
}

