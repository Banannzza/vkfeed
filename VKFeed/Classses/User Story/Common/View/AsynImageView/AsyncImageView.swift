//
//  AsyncImageView.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class AsyncImageView: UIImageView {
    
    private var imageDownloadTask: URLSessionDataTask?
    
    private static let workQueue = DispatchQueue(label: "AsyncImageView.WorkQueue", qos: .userInitiated)
    
    static func downsample(imageFrom imageData: Data, to pointSize: CGSize, scale: CGFloat) -> UIImage {
        let imageSourceOptions = [kCGImageSourceShouldCache: false] as CFDictionary
        let imageSource = CGImageSourceCreateWithData(imageData as CFData, imageSourceOptions)!
        let maxDimensionInPixels = max(pointSize.width, pointSize.height) * scale
        let downsampleOptions =
            [kCGImageSourceCreateThumbnailFromImageAlways: true,
             kCGImageSourceShouldCacheImmediately: true,
             kCGImageSourceCreateThumbnailWithTransform: true,
             kCGImageSourceThumbnailMaxPixelSize: maxDimensionInPixels] as CFDictionary
        let downsampledImage =
            CGImageSourceCreateThumbnailAtIndex(imageSource, 0, downsampleOptions)!
        return UIImage(cgImage: downsampledImage)
    }
    
    private func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        DispatchQueue.global().async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.imageDownloadTask?.cancel()
            strongSelf.imageDownloadTask = URLSession.shared.dataTask(with: url, completionHandler: completion)
            strongSelf.imageDownloadTask?.resume()
        }
    }
    
    func cancelLoading() {
        imageDownloadTask?.cancel()
    }
    
    func asyncSetImage(from url: URL) {
        if let image = ImageManager.shared.imageData(for: url) {
            self.image = image
            return
        }
        let currentSize = frame.size
        getData(from: url) { (data, _, _) in
            AsyncImageView.workQueue.async { [weak self] in
                guard let strongSelf = self else { return }
                guard let data = data else { return }
                let image = AsyncImageView.downsample(imageFrom: data, to: currentSize, scale: UIScreen.main.scale)
                
                DispatchQueue.main.async {
                    strongSelf.image = image
                }
            }
        }
    }
    
    func asyncSetImage(from photo: PhotoAttachment, forSize: CGSize) {
        guard let url = URL(string: photo.sizes.last?.url ?? "") else { return}
        
        if let image = ImageManager.shared.imageData(for: url) {
            self.image = image
            return
        }
        
        getData(from: url) { (data, _, _) in
            AsyncImageView.workQueue.async { [weak self] in
                guard let strongSelf = self else { return }
                guard let data = data else { return }
                let image = AsyncImageView.downsample(imageFrom: data, to: forSize, scale: UIScreen.main.scale)
                ImageManager.shared.save(data: image, forURL: url)
                DispatchQueue.main.async {
                    strongSelf.image = image
                }
            }
            
        }
    }
    
    func asyncSetAvatarImage(from url: URL, backgroundColor: UIColor? = .white) {
        if let image = ImageManager.shared.imageData(for: url) {
            self.image = image
            return
        }
        
        let currentSize = frame.size
        
        getData(from: url) { (data, _, _) in
            AsyncImageView.workQueue.async { [weak self] in
                guard let strongSelf = self else { return }
                guard let data = data else { return }
                let image = AsyncImageView.downsample(imageFrom: data, to: currentSize, scale: UIScreen.main.scale)
                let avatarImage = UIImage.makeAvatatImage(from: image, aspectFilledFor: currentSize, backgroundColor: backgroundColor)!
                ImageManager.shared.save(data: avatarImage, forURL: url)
                DispatchQueue.main.async {
                    strongSelf.image = avatarImage
                }
            }
        }
    }
    
}
