//
//  PostActionBarView.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class PostActionBarView: UIView {
    
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var likeLabel: UILabel!
    @IBOutlet weak var commentImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var shareLabel: UILabel!
    @IBOutlet weak var seenImageView: UIImageView!
    @IBOutlet weak var seenLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        likeImageView.tintColor = UIColor.imageGrayTintColor
        commentImageView.tintColor = UIColor.imageGrayTintColor
        shareImageView.tintColor = UIColor.imageGrayTintColor
        seenImageView.tintColor = UIColor.imageGrayTintColor
    }
    
}
