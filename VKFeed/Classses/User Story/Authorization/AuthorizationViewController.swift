//
//  AuthorizationViewController.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit
import VK_ios_sdk

class AuthorizationViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        VKSdk.forceLogout()

        if !VKSdk.initialized() {
            let vkInstance = VKSdk.initialize(withAppId: "6746248")
            vkInstance?.uiDelegate = self
            vkInstance?.register(self)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        VKSdk.authorize(["wall", "friends"])
    }
    
}

extension AuthorizationViewController: VKSdkDelegate {
    func vkSdkUserAuthorizationFailed() {

    }
    
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        if let token = result?.token {
            let vcIdentifier = String(describing: FeedViewController.self)
            let vc = UIStoryboard(name: vcIdentifier, bundle: nil).instantiateInitialViewController() as! FeedViewController
            ServiceLayer.shared.accessToken = token.accessToken
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension AuthorizationViewController: VKSdkUIDelegate {
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        present(controller, animated: true, completion: nil)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        if let vc = VKCaptchaViewController.captchaControllerWithError(captchaError) {
            present(vc, animated: true, completion: nil)
        }
    }
}
