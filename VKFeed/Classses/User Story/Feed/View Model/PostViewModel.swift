//
//  PostViewModel.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct PostViewModel: Equatable {
    let id: Int
    let likesCount: Int
    let shareCount: Int
    let commentCount: Int
    let seenCount: String
    let authorName: String
    let dateString: String
    let text: String?
    let authorImageURLString: String?
    let photo: [PhotoAttachment]?
    
    init(_ model: Post, author: String?, avatarURLString: String?) {
        id = model.id
        likesCount = model.likes.count
        shareCount = model.reposts.count
        commentCount = model.comments.count
        seenCount = model.views.count > 10000 ? "\(model.views.count / 1000)K" : "\(model.views.count)"
        text = model.text
        authorName = author ?? ""
        authorImageURLString = avatarURLString
        photo = model.attachments?.compactMap { $0.photo }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "RU")
        dateFormatter.dateFormat = "d MMM в hh:mm"
        
        self.dateString = dateFormatter.string(from: model.date)
    }
    
    static func == (lhs: PostViewModel, rhs: PostViewModel) -> Bool {
        return lhs.id == rhs.id
    }

}
