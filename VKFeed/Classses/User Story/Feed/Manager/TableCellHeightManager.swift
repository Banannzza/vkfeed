//
//  TableCellHeightManager.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class TableCellHeightManager {
    
    // MARK: - Property
    
    private var cachedHeight = [Int: CGFloat]()
    private var cells = [String: FeedTableViewCellProtocol & UITableViewCell]()
    
    // MARK: - Method
    
    func register(cell: FeedTableViewCellProtocol & UITableViewCell, withIdentifier identifier: String) {
        cells[identifier] = cell
    }
    
    func height(forIdentifier identifier: String, withModel model: PostViewModel) -> CGFloat {
        if let cached = cachedHeight[model.id] {
            return cached
        }
        let height = ceil(cells[identifier]?.heightThatFits(viewMode: model) ?? -1)
        cachedHeight[model.id] = height
        return height
    }
    
    func clear() {
        cachedHeight.removeAll()
    }
    
}
