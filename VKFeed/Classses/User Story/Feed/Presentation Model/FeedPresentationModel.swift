//
//  FeedPresentationModel.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol FeedPresentationModelDelegate: AnyObject {
    func presentationModelDidLoadPosts(_ model: FeedPresentationModel)
    func presentationModel(_ model: FeedPresentationModel, didLoadNextPosts: Int, prevCount: Int, completion: @escaping () -> Void)
    func presentationModel(_ model: FeedPresentationModel, didLoadUserImageURL: URL)
}


class FeedPresentationModel {
    
    enum Mode {
        case search
        case regular
    }
    
    // MARK: - Property
    
    private let postCount: Int? = nil
    
    var mode = Mode.regular
    private var posts = [PostViewModel]()
    private var searchPosts = [PostViewModel]()
    private var groups = [Int: Group]()
    private var profiles = [Int: Profile]()
    
    unowned let delegate: FeedPresentationModelDelegate
    
    // MARK: - Init
    
    init(delegate: FeedPresentationModelDelegate) {
        self.delegate = delegate
    }
    
    // MARK: - Private
    
    private func convertToViewModels(newPosts: [Post]) -> [PostViewModel] {
        var result = [PostViewModel]()
        
        for post in newPosts {
            var viewModel: PostViewModel!
            if post.sourceID < 0, let group = groups[abs(post.sourceID)] {
                viewModel = PostViewModel(post, author: group.name, avatarURLString: group.imageURLString)
            }
            else if let profile = profiles[post.sourceID] {
                viewModel = PostViewModel(post, author: profile.firstName + " " + profile.lastName, avatarURLString: profile.imageURLString)
            }
            else {
                viewModel = PostViewModel(post, author: nil, avatarURLString: nil)
            }
            result.append(viewModel)
        }
        return result
    }
    
    private func updateGroups(newGroups: [Group]) {
        for group in newGroups where groups[group.id] == nil {
            groups[group.id] = group
        }
    }
    
    private func updateProfiles(newProfiles: [Profile]) {
        for profile in newProfiles where profiles[profile.id] == nil {
            profiles[profile.id] = profile
        }
    }
    
    private func appendNewPosts(_ newPosts: [PostViewModel]) {
        switch mode {
        case .regular:
            posts.append(contentsOf: newPosts)
        case .search:
            searchPosts.append(contentsOf: newPosts)
        }
    }
    
    // MARK: - Public
    
    func numberOfSections() -> Int {
        switch mode {
        case .regular:
            return posts.count > 0 ? 1 : 0
        case .search:
            return searchPosts.count > 0 ? 1 : 0
        }
    }
    
    func numberOfPosts() -> Int {
        switch mode {
        case .regular:
            return posts.count
        case .search:
            return searchPosts.count
        }
    }
    
    func post(for index: Int) -> PostViewModel {
        switch mode {
        case .regular:
            return posts[index]
        case .search:
            return searchPosts[index]
        }
    }
    
    func loadPosts() {
        ServiceLayer.shared.newsfeedService.loadLastPosts(count: postCount) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .fail:
                break
            case .success(let response):
                strongSelf.updateGroups(newGroups: response.groups)
                strongSelf.updateProfiles(newProfiles: response.profiles)
                strongSelf.posts = strongSelf.convertToViewModels(newPosts: response.posts)
                strongSelf.delegate.presentationModelDidLoadPosts(strongSelf)
            }
        }
    }
    
    func loadUserAvatar() {
        ServiceLayer.shared.usersService.userImageURL(forUser: nil) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .fail:
                break
            case .success(let response):
                strongSelf.delegate.presentationModel(strongSelf, didLoadUserImageURL: response)
            }
        }
    }
    
    func loadNextPosts() {
        guard posts.count > 0 else {
            loadPosts()
            return
        }
        ServiceLayer.shared.newsfeedService.loadNextPosts(count: postCount) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .fail:
                break
            case .success(let response):
                strongSelf.updateGroups(newGroups: response.groups)
                strongSelf.updateProfiles(newProfiles: response.profiles)
                let newPosts = strongSelf.convertToViewModels(newPosts: response.posts)
                strongSelf.delegate.presentationModel(strongSelf, didLoadNextPosts: newPosts.count, prevCount: strongSelf.posts.count, completion: {
                   strongSelf.posts.append(contentsOf: newPosts)
                })
                

            }
        }
    }
    
    func loadNewPosts() {
        posts.removeAll()
        loadPosts()
    }
    
}
