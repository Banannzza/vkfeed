//
//  PostAttachmentTableViewCell.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class PostAttachmentTableViewCell: PostTableViewCell {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var attachmentContainer: UIView!
    @IBOutlet weak var collectionView: PhotoCollectionView!
    
    // MARK: - Property
    
    lazy var attachmentContainerHeight = floor((UIScreen.main.bounds.width - 16) / 1.3)
    
    // MARK: - FeedTableViewCellProtocol
    
    override func configure(from viewModel: PostViewModel) {
        super.configure(from: viewModel)
        collectionView.reloadPhotos(with: viewModel.photo ?? [])
    }
    
    override func heightThatFits(viewMode: PostViewModel) -> CGFloat {
        let partHeight = super.heightThatFits(viewMode: viewMode)

        let totalHeight = partHeight + attachmentContainerHeight
        
        return totalHeight
    }
}

