//
//  FeedTableViewCellProtocol.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

protocol FeedTableViewCellProtocol where Self: UITableViewCell {
    func configure(from viewModel: PostViewModel)
    func heightThatFits(viewMode: PostViewModel) -> CGFloat
}

