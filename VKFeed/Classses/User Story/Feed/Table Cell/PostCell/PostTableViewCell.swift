//
//  PostTableViewCell.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell, FeedTableViewCellProtocol {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var postView: UIView!
    @IBOutlet weak var postHeaderView: UIView!
    @IBOutlet weak var postDescriptionLabel: UILabel!
    @IBOutlet weak var postActionView: UIView!
    @IBOutlet var heightImportantConstraints: [NSLayoutConstraint]!
    
    // MARK: - Property
    
    lazy var totalHeightImportantConstraintsHeight: CGFloat = heightImportantConstraints!.reduce(0.0) { $0 + $1.constant }
    
    var headerView: PostHeaderView!
    var actionView: PostActionBarView!

    // MARK: - Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        headerView.authorImageView.cancelLoading()
        headerView.authorLabel.text = nil
        headerView.authorImageView.image = nil
        postDescriptionLabel.text = nil
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let header = PostHeaderView.loadViewFromNib() as! PostHeaderView
        header.addAsSubview(for: postHeaderView)
        headerView = header
        
        let footer = PostActionBarView.loadViewFromNib() as! PostActionBarView
        footer.addAsSubview(for: postActionView)
        actionView = footer
    }
    
    // MARK: - FeedTableViewCellProtocol
    
    func configure(from viewModel: PostViewModel) {
        actionView.likeLabel.text = String(viewModel.likesCount)
        actionView.commentLabel.text = String(viewModel.commentCount)
        actionView.shareLabel.text = String(viewModel.shareCount)
        actionView.seenLabel.text = String(viewModel.seenCount)
        postDescriptionLabel.text = viewModel.text
        headerView.authorLabel.text = viewModel.authorName
        headerView.dateLabel.text = viewModel.dateString
        
        if let imageURLString = viewModel.authorImageURLString, let url = URL(string: imageURLString) {
            headerView.authorImageView.asyncSetAvatarImage(from: url)
        }
    }
    
    func heightThatFits(viewMode: PostViewModel) -> CGFloat {
        let headerHeight: CGFloat = 36
        let footerHeight: CGFloat = 30
        
        postDescriptionLabel.text = viewMode.text
        postDescriptionLabel.sizeToFit()
        let textHeight = postDescriptionLabel.frame.height//viewMode.text?.height(withConstrainedWidth: UIScreen.main.bounds.width - 40, font: postDescriptionLabel.font) ?? 0
        
        let contentHeight = headerHeight + footerHeight + textHeight
        let totalHeight = contentHeight + totalHeightImportantConstraintsHeight
        
        return totalHeight
    }
    
}
