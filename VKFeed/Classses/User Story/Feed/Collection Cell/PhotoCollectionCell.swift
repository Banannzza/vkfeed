//
//  PhotoCollectionCell.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class PhotoCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: AsyncImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.cancelLoading()
        photoImageView.image = nil
    }
    
    func configure(photo: PhotoAttachment, size: CGSize) {
        photoImageView.asyncSetImage(from: photo, forSize: size)
    }
}
