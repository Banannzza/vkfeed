//
//  FeedViewController.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit
import VK_ios_sdk

class FeedViewController: UIViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Property
    
    lazy var presentationModel = FeedPresentationModel(delegate: self)
    
    private let tableHeaderHeight: CGFloat = 80
    private let tableFooterHeight: CGFloat = 64
    private let heightManager = TableCellHeightManager()
    
    var infinyLoadingEnabled = true
    var contentOffsetObserver: NSKeyValueObservation?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor(red: 0.32, green: 0.51, blue: 0.72, alpha: 1.0)
        return refreshControl
    }()
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        configureUI()
        
        presentationModel.loadPosts()
        presentationModel.loadUserAvatar()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
        sizeFooterToFit()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addObserving()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        contentOffsetObserver?.invalidate()
    }
    
    override func didReceiveMemoryWarning() {
        ImageManager.shared.clear()
        heightManager.clear()
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Configure
    
    private func configureUI() {
        configureGradientLayer()
        
        tableView.addSubview(refreshControl)
        
        let header = TableHeaderView.loadViewFromNib() as! TableHeaderView
        tableView.tableHeaderView = header
        
        let footer = TableFooterView.loadViewFromNib()
        tableView.tableFooterView = footer
    }
    
    private func configureTableView() {
        tableView.estimatedRowHeight = 0
        tableView.estimatedSectionFooterHeight = 0
        tableView.estimatedSectionHeaderHeight = 0
        tableView.tableFooterView = UIView()
        PostTableViewCell.register(to: tableView)
        PostAttachmentTableViewCell.register(to: tableView)
        heightManager.register(cell: PostTableViewCell.loadViewFromNib() as! UITableViewCell & FeedTableViewCellProtocol, withIdentifier: String(describing: PostTableViewCell.self))
        heightManager.register(cell: PostAttachmentTableViewCell.loadViewFromNib() as! UITableViewCell & FeedTableViewCellProtocol, withIdentifier: String(describing: PostAttachmentTableViewCell.self))
    }
    
    private func configureGradientLayer() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [
            UIColor(red: 0.97, green: 0.97, blue: 0.98, alpha: 1).cgColor,
            UIColor(red: 0.92, green: 0.93, blue: 0.94, alpha: 1).cgColor
        ]
        gradientLayer.locations = [0, 1]
        gradientLayer.startPoint = CGPoint(x: 0.25, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.75, y: 0.5)
        gradientLayer.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 0.5, ty: 0))
        gradientLayer.frame = UIScreen.main.bounds
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    private func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        var frame = headerView.frame
        if tableHeaderHeight != headerView.frame.height {
            frame.size.height = tableHeaderHeight
            headerView.frame = frame
            tableView.tableHeaderView = headerView
        }
    }
    
    private func sizeFooterToFit() {
        let footerView = tableView.tableFooterView!
        var frame = footerView.frame
        if tableFooterHeight != footerView.frame.height {
            frame.size.height = tableFooterHeight
            footerView.frame = frame
            tableView.tableFooterView = footerView
        }
    }
    
    // MARK: - Observing
    
    func addObserving() {
        contentOffsetObserver = tableView.observe(\.contentOffset, options: .new) { _, _ in
            self.checkScrollOffset()
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        DispatchQueue.main.async {
            self.presentationModel.loadNewPosts()
            self.tableView.beginUpdates()
            self.tableView.deleteSections([0], with: .fade)
            self.tableView.endUpdates()
            if let footer = self.tableView.tableFooterView as? TableFooterView {
                footer.updatePostCounter(0)
            }
        }
    }
    
    func checkScrollOffset() {
        let currentOffset = tableView.contentOffset.y
        let maximumOffset = tableView.contentSize.height - tableView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        if deltaOffset <= tableFooterHeight, infinyLoadingEnabled {
            infinyLoadingEnabled = false
            self.presentationModel.loadNextPosts()
            if let footer = self.tableView.tableFooterView as? TableFooterView {
                footer.setAnimation(true)
            }
        }
       
        self.headerView.alpha = min(currentOffset, 2) / 2.0  

    }
}

// MARK: - UITableViewDataSource

extension FeedViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presentationModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.numberOfPosts()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let post = presentationModel.post(for: indexPath.row)
        let cellIdentifier = post.photo?.count ?? 0 == 0 ? String(describing: PostTableViewCell.self) : String(describing: PostAttachmentTableViewCell.self)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FeedTableViewCellProtocol & UITableViewCell
        cell.configure(from: post)
        return cell
    }
}

// MARK: - UITableViewDelegate

extension FeedViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let post = presentationModel.post(for: indexPath.row)
        let cellIdentifier = post.photo?.count ?? 0 == 0 ? String(describing: PostTableViewCell.self) : String(describing: PostAttachmentTableViewCell.self)
        return heightManager.height(forIdentifier: cellIdentifier, withModel: post)
    }
    
}

// MARK: - FeedPresentationModelDelegate

extension FeedViewController: FeedPresentationModelDelegate {
    
    func presentationModel(_ model: FeedPresentationModel, didLoadUserImageURL: URL) {
        DispatchQueue.main.async {
            if let header = self.tableView.tableHeaderView as? TableHeaderView {
                header.avatarImageView.asyncSetAvatarImage(from: didLoadUserImageURL, backgroundColor: .clear)
            }
        }
    }
    
    func presentationModelDidLoadPosts(_ model: FeedPresentationModel) {
        DispatchQueue.main.async {
            self.refreshControl.endRefreshing()
            self.tableView.beginUpdates()
            self.tableView.insertSections([0], with: .fade)
            self.tableView.endUpdates()
            if let footer = self.tableView.tableFooterView as? TableFooterView {
                footer.updatePostCounter(self.presentationModel.numberOfPosts())
            }
        }
    }
    
    func presentationModel(_ model: FeedPresentationModel, didLoadNextPosts: Int, prevCount: Int, completion: @escaping () -> Void) {
        var indicies = [IndexPath]()
        for index in prevCount..<(prevCount + didLoadNextPosts) {
            indicies.append(IndexPath(row: index, section: 0))
        }
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0, animations: {
                self.tableView.beginUpdates()
                completion()
                self.tableView.insertRows(at: indicies, with: .fade)
                self.tableView.endUpdates()
            }, completion: { _ in
                self.infinyLoadingEnabled = true
                if let footer = self.tableView.tableFooterView as? TableFooterView {
                    footer.updatePostCounter(didLoadNextPosts + prevCount)
                }
            })
        }
        
    }
    
}
