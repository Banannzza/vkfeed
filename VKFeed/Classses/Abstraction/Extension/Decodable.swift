//
//  Decodable.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

extension Decodable {
    
    static func decode(from array: [[String: Any]]) -> [Self] {
        var objects = [Self]()
        for dictionary in array {
            objects.append(contentsOf: decode(from: dictionary))
        }
        return objects
    }
    
    static func decode(from json: [String: Any]) -> [Self] {
        var objects = [Self]()
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []), let object = try? JSONDecoder().decode(Self.self, from: jsonData) {
            return [object]
        } else {
            for key in json.keys {
                if let dictionary = json[key] as? [String: Any] {
                    objects.append(contentsOf: decode(from: dictionary))
                }
                if let array = json[key] as? [[String: Any]] {
                    for dictionary in array {
                        objects.append(contentsOf: decode(from: dictionary))
                    }
                }
            }
        }
        return objects
    }
    
}
