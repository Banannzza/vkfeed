//
//  PhotoSize.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct PhotoSize: Decodable {
    let type: String
    let url: String
    let width: Int
    let height: Int
}
