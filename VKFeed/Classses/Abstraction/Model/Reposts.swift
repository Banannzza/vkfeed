//
//  Reposts.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct Reposts: Decodable {
    let count: Int
    let userReposted: Bool
    
    enum CodingKeys: String, CodingKey {
        case count
        case userReposted = "user_reposted"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        count = try container.decode(Int.self, forKey: .count)
        userReposted = try container.decode(Int.self, forKey: .userReposted) == 1 ? true : false
    }
    
}
