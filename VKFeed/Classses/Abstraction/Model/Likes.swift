//
//  Likes.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct Likes: Decodable {
    let count: Int
    let canLike: Bool
    
    enum CodingKeys: String, CodingKey {
        case count
        case canLike = "can_like"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        count = try container.decode(Int.self, forKey: .count)
        canLike = try container.decode(Int.self, forKey: .canLike) == 1 ? true : false
    }
}
