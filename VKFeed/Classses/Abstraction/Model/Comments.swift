//
//  Comments.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct Comments: Decodable {
    let count: Int
    let canPost: Bool
    
    enum CodingKeys: String, CodingKey {
        case count
        case canPost = "can_post"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        count = try container.decode(Int.self, forKey: .count)
        canPost = try container.decode(Int.self, forKey: .canPost) == 1 ? true : false
    }

}
