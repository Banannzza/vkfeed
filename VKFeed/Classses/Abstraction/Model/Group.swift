//
//  Group.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class Group: NSObject, Decodable {
    let id: Int
    let name: String
    let imageURLString: String
    
    init(id: Int, name: String, imageURLString: String) {
        self.id = id
        self.name = name
        self.imageURLString = imageURLString
    }
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case imageURLString = "photo_200"
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let otherGroup = object as? Group else { return false }
        return id == otherGroup.id
    }
}
