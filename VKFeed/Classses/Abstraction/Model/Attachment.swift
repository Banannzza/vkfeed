//
//  Attachment.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct Attachment: Decodable {
    let photo: PhotoAttachment?
}
