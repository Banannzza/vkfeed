//
//  Post.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 09/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct Post: Decodable {
    let id: Int
    let sourceID: Int
    let text: String?
    var date: Date {
        return Date(timeIntervalSince1970: timestamp)
    }
    let timestamp: Double
    let comments: Comments
    let likes: Likes
    let reposts: Reposts
    let views: Views
    let attachments: [Attachment]?
    
    enum CodingKeys: String, CodingKey {
        case id = "post_id"
        case sourceID = "source_id"
        case text
        case timestamp = "date"
        case comments
        case likes
        case reposts
        case views
        case attachments
    }
    
}
