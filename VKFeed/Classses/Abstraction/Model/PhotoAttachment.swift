//
//  PhotoAttachment.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 11/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct PhotoAttachment: Decodable, Equatable {
    
    static func == (lhs: PhotoAttachment, rhs: PhotoAttachment) -> Bool {
        return lhs.id == rhs.id
    }
    
    let id: Int
    let sizes: [PhotoSize]
    
}
