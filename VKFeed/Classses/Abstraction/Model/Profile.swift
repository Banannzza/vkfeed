//
//  Profile.swift
//  VKFeed
//
//  Created by Алексей Остапенко on 10/11/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct Profile: Decodable {
    let id: Int
    let firstName: String
    let lastName: String
    let imageURLString: String
    
    
    enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case imageURLString = "photo_100"
    }
}
